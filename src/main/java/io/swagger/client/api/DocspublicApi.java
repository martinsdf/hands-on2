/**
 * Marvel
 * Marvel Comics
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;
import io.swagger.client.model.CharacterDataWrapper;

public class DocspublicApi {
    private ApiClient apiClient;

    public DocspublicApi() {
        this(Configuration.getDefaultApiClient());
    }

    public DocspublicApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for getCharacterIndividual */
    private com.squareup.okhttp.Call getCharacterIndividualCall(BigDecimal characterId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'characterId' is set
        if (characterId == null) {
            throw new ApiException("Missing the required parameter 'characterId' when calling getCharacterIndividual(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/v1/public/characters/{characterId}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "characterId" + "\\}", apiClient.escapeString(characterId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Fetches a single character by id.
     * This method fetches a single character resource.  It is the canonical URI for any character resource provided by the API.
     * @param characterId A single character id. (required)
     * @return CharacterDataWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CharacterDataWrapper getCharacterIndividual(BigDecimal characterId) throws ApiException {
        ApiResponse<CharacterDataWrapper> resp = getCharacterIndividualWithHttpInfo(characterId);
        return resp.getData();
    }

    /**
     * Fetches a single character by id.
     * This method fetches a single character resource.  It is the canonical URI for any character resource provided by the API.
     * @param characterId A single character id. (required)
     * @return ApiResponse&lt;CharacterDataWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CharacterDataWrapper> getCharacterIndividualWithHttpInfo(BigDecimal characterId) throws ApiException {
        com.squareup.okhttp.Call call = getCharacterIndividualCall(characterId, null, null);
        Type localVarReturnType = new TypeToken<CharacterDataWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fetches a single character by id. (asynchronously)
     * This method fetches a single character resource.  It is the canonical URI for any character resource provided by the API.
     * @param characterId A single character id. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCharacterIndividualAsync(BigDecimal characterId, final ApiCallback<CharacterDataWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getCharacterIndividualCall(characterId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CharacterDataWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for getCreatorCollection */
    private com.squareup.okhttp.Call getCreatorCollectionCall(String name, String nameStartsWith, String modifiedSince, List<String> comics, List<String> series, List<String> events, List<String> stories, List<String> orderBy, BigDecimal limit, BigDecimal offset, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        

        // create path and map variables
        String localVarPath = "/v1/public/characters".replaceAll("\\{format\\}","json");

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (name != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));
        if (nameStartsWith != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "nameStartsWith", nameStartsWith));
        if (modifiedSince != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "modifiedSince", modifiedSince));
        if (comics != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("csv", "comics", comics));
        if (series != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("csv", "series", series));
        if (events != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("csv", "events", events));
        if (stories != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("csv", "stories", stories));
        if (orderBy != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("csv", "orderBy", orderBy));
        if (limit != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "limit", limit));
        if (offset != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "offset", offset));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Fetches lists of characters.
     * Fetches lists of comic characters with optional filters. See notes on individual parameters below.
     * @param name Return only characters matching the specified full character name (e.g. Spider-Man). (optional)
     * @param nameStartsWith Return characters with names that begin with the specified string (e.g. Sp). (optional)
     * @param modifiedSince Return only characters which have been modified since the specified date. (optional)
     * @param comics Return only characters which appear in the specified comics (accepts a comma-separated list of ids). (optional)
     * @param series Return only characters which appear the specified series (accepts a comma-separated list of ids). (optional)
     * @param events Return only characters which appear in the specified events (accepts a comma-separated list of ids). (optional)
     * @param stories Return only characters which appear the specified stories (accepts a comma-separated list of ids). (optional)
     * @param orderBy Order the result set by a field or fields. Add a \&quot;-\&quot; to the value sort in descending order. Multiple values are given priority in the order in which they are passed. (optional)
     * @param limit Limit the result set to the specified number of resources. (optional)
     * @param offset Skip the specified number of resources in the result set. (optional)
     * @return CharacterDataWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CharacterDataWrapper getCreatorCollection(String name, String nameStartsWith, String modifiedSince, List<String> comics, List<String> series, List<String> events, List<String> stories, List<String> orderBy, BigDecimal limit, BigDecimal offset) throws ApiException {
        ApiResponse<CharacterDataWrapper> resp = getCreatorCollectionWithHttpInfo(name, nameStartsWith, modifiedSince, comics, series, events, stories, orderBy, limit, offset);
        return resp.getData();
    }

    /**
     * Fetches lists of characters.
     * Fetches lists of comic characters with optional filters. See notes on individual parameters below.
     * @param name Return only characters matching the specified full character name (e.g. Spider-Man). (optional)
     * @param nameStartsWith Return characters with names that begin with the specified string (e.g. Sp). (optional)
     * @param modifiedSince Return only characters which have been modified since the specified date. (optional)
     * @param comics Return only characters which appear in the specified comics (accepts a comma-separated list of ids). (optional)
     * @param series Return only characters which appear the specified series (accepts a comma-separated list of ids). (optional)
     * @param events Return only characters which appear in the specified events (accepts a comma-separated list of ids). (optional)
     * @param stories Return only characters which appear the specified stories (accepts a comma-separated list of ids). (optional)
     * @param orderBy Order the result set by a field or fields. Add a \&quot;-\&quot; to the value sort in descending order. Multiple values are given priority in the order in which they are passed. (optional)
     * @param limit Limit the result set to the specified number of resources. (optional)
     * @param offset Skip the specified number of resources in the result set. (optional)
     * @return ApiResponse&lt;CharacterDataWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CharacterDataWrapper> getCreatorCollectionWithHttpInfo(String name, String nameStartsWith, String modifiedSince, List<String> comics, List<String> series, List<String> events, List<String> stories, List<String> orderBy, BigDecimal limit, BigDecimal offset) throws ApiException {
        com.squareup.okhttp.Call call = getCreatorCollectionCall(name, nameStartsWith, modifiedSince, comics, series, events, stories, orderBy, limit, offset, null, null);
        Type localVarReturnType = new TypeToken<CharacterDataWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fetches lists of characters. (asynchronously)
     * Fetches lists of comic characters with optional filters. See notes on individual parameters below.
     * @param name Return only characters matching the specified full character name (e.g. Spider-Man). (optional)
     * @param nameStartsWith Return characters with names that begin with the specified string (e.g. Sp). (optional)
     * @param modifiedSince Return only characters which have been modified since the specified date. (optional)
     * @param comics Return only characters which appear in the specified comics (accepts a comma-separated list of ids). (optional)
     * @param series Return only characters which appear the specified series (accepts a comma-separated list of ids). (optional)
     * @param events Return only characters which appear in the specified events (accepts a comma-separated list of ids). (optional)
     * @param stories Return only characters which appear the specified stories (accepts a comma-separated list of ids). (optional)
     * @param orderBy Order the result set by a field or fields. Add a \&quot;-\&quot; to the value sort in descending order. Multiple values are given priority in the order in which they are passed. (optional)
     * @param limit Limit the result set to the specified number of resources. (optional)
     * @param offset Skip the specified number of resources in the result set. (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCreatorCollectionAsync(String name, String nameStartsWith, String modifiedSince, List<String> comics, List<String> series, List<String> events, List<String> stories, List<String> orderBy, BigDecimal limit, BigDecimal offset, final ApiCallback<CharacterDataWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getCreatorCollectionCall(name, nameStartsWith, modifiedSince, comics, series, events, stories, orderBy, limit, offset, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CharacterDataWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
