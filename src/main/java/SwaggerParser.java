import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerCompatConverter;
import io.swagger.util.Json;

public class SwaggerParser {

	
	public static void main(String[] args) throws IOException
	{
		// read a swagger description from the petstore
		
		Swagger swagger = new SwaggerCompatConverter().read("C:\\marvel\\hands-on\\swagger.json");
	
		String swaggerString = Json.pretty(swagger);
		
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream("C:\\marvel\\hands-on\\swagger2.json"), "utf-8"));
		    writer.write(swaggerString);
		} catch (IOException ex) {
		  // report
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		}
	}
}
